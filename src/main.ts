/// <reference path="main.d.ts" />
import PlayerControlsModule from './modules/playerControlsModule.js'
import MapModule from './modules/mapModule.js'

console.log('Javascript loaded succesfully')

const canvas: HTMLCanvasElement = document.getElementById('canvas') as HTMLCanvasElement
canvas.width = document.body.clientWidth
canvas.height = document.body.clientHeight

const updateCanvas = (context: CanvasRenderingContext2D, mapModule: MapModule, playerCtrlModule: PlayerControlsModule): void => {
    if (mapModule.playerCollideWithSomething(
        playerCtrlModule.playerPosition,
        playerCtrlModule.playerSize)) {
            playerCtrlModule.respawn()
    }
    context.clearRect(0, 0, 800, 600)
    mapModule.drawMap()
    playerCtrlModule.playerMove()
    context.fillStyle = 'rgb(254,254,254)'
    context.fillRect(
        playerCtrlModule.playerPosition.x,
        playerCtrlModule.playerPosition.y,
        playerCtrlModule.playerSize.width,
        playerCtrlModule.playerSize.height
    )
}

if (canvas.getContext) {
    const canvasContext: CanvasRenderingContext2D = canvas.getContext('2d')
    const playerCtrlModule = new PlayerControlsModule(window, canvasContext)
    const mapModule = new MapModule(window, canvasContext)
    setInterval(updateCanvas.bind(this, canvasContext, mapModule, playerCtrlModule), 5)
}