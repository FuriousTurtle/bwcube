export default class MapModule {
    
    public wallsLevel1: Array<{ x: number, y: number, width: number, height: number } | {
        startX: number,
        startY: number, endX: number, endY: number, thickness: number
    }> = []
    
    public movingWall1: bwCube.MovingWall = {
        x: 400, initX: 400, y: 500, initY: 500, width: 80,
        height: 5, distance: 115, start: true
    }
    
    public movingWall2: bwCube.MovingWall = {
        x: 50, initX: 50, y: 100, initY: 100, width: 80,
        height: 5, distance: 68, start: true
    } 
    
    public mapBounds: {xLeft: number, xRight: number, yUp: number, yDown: number} =
    {xLeft: 0, xRight: 770, yUp: 0, yDown: 570}
    
    constructor(public window: Window, public ctx: any) { }
    
    setWall = (wall: bwCube.BasicWall, temporary: boolean = false): void => {
        this.ctx.fillStyle = 'rgb(255,255,255)'
        this.ctx.fillRect(wall.x, wall.y, wall.width, wall.height)
        if (!temporary) this.wallsLevel1.push(wall)
    }
    
    setDiagonal = (wall: bwCube.DiagonalWall): void => {
        this.ctx.strokeStyle = 'rgb(255,255,255)'
        this.ctx.beginPath()
        this.ctx.moveTo(wall.startX, wall.startY)
        this.ctx.lineTo(wall.endX, wall.endY)
        this.ctx.lineWidth = wall.thickness
        this.ctx.stroke()
        this.wallsLevel1.push(wall)
    }
    
    setHorizontalMovingWall = (movingWall: bwCube.MovingWall): void => {
        if (movingWall.x >= movingWall.initX + movingWall.distance) movingWall.start = false;
        if (movingWall.x <= movingWall.initX) movingWall.start = true;
        movingWall.start ? movingWall.x++ : movingWall.x--;
        
        this.setWall({ x: movingWall.x, y: movingWall.y, width: movingWall.width, height: movingWall.height }, true)
    }
    
    setWinningZone = (winningZone: bwCube.WinningZone): void => {
        this.ctx.fillStyle = 'rgb(0, 255, 0)'
        this.ctx.fillRect(winningZone.x, winningZone.y, winningZone.width, winningZone.height)
    }

    playerCollideWithSomething = (playerPosition: bwCube.PlayerPosition, playerSize: bwCube.PlayerSize): boolean => {
            for(let color of this.ctx.getImageData(playerPosition.x - 2, playerPosition.y, -1,
              playerSize.width - 2).data) {
                if(color == 255) return true
            }
            for(let color of this.ctx.getImageData(playerPosition.x + playerSize.width + 2, playerPosition.y, 1,
              playerSize.width - 2).data) {
                if(color == 255) return true
            }
            for(let color of this.ctx.getImageData(playerPosition.x, playerPosition.y + playerSize.height + 2,
              playerSize.height - 2, 1).data){
                if(color == 255) return true
            }
            for(let color of this.ctx.getImageData(playerPosition.x, playerPosition.y - 2,
              playerSize.height - 2, -1).data){
                if(color == 255) return true
            }
            return false
    }
    
    drawMap = (): void => {
        this.setWall({ x: 50, y: 0, width: 5, height: 500 })
        this.setWall({ x: 100, y: 600, width: 5, height: -500 })
        this.setWall({ x: 192, y: 0, width: 5, height: 106 })
        this.setDiagonal({ startX: 103, startY: 103, endX: 600, endY: 600, thickness: 5 })
        this.setDiagonal({ startX: 193, startY: 103, endX: 620, endY: 530, thickness: 5 })
        this.setHorizontalMovingWall(this.movingWall1)
        this.setHorizontalMovingWall(this.movingWall2)
        this.setWinningZone({ x: 750, y: 0, width: 45, height: 45 })
    }
    
}
