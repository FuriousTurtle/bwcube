export default class PlayerControlsModule {

  private readonly leftArrow = "ArrowLeft"
  private readonly upArrow = "ArrowUp"
  private readonly rightArrow = "ArrowRight"
  private readonly downArrow = "ArrowDown"
  private readonly godmode = false // for dev

  public pMoveLeft: boolean
  public pMoveUp: boolean
  public pMoveRight: boolean
  public pMoveDown: boolean

  public readonly playerSize = {width: 25, height: 25}
  public readonly playerSpeed = 2
  public mapBounds = {xLeft: 0, xRight: 770, yUp: 0, yDown: 570}
  public playerLocked = false

  public playerPosition: bwCube.PlayerPosition = {x: 10, y: 10}

  constructor(public window: Window, public ctx: any)
  {
    window.addEventListener("keyup", (event: KeyboardEvent) => {
        event.preventDefault()
        switch(event.key) {
            case this.leftArrow :
                this.pMoveLeft = false
                break
            case this.upArrow :
                this.pMoveUp = false
                break
            case this.rightArrow :
                this.pMoveRight = false
                break
            case this.downArrow :
                this.pMoveDown = false
                break
        }
    })

    window.addEventListener("keydown", (event: KeyboardEvent) => {
        event.preventDefault()
        if (this.playerLocked == false) {
            switch(event.key) {
                case this.leftArrow :
                    this.pMoveLeft = true
                    break
                case this.upArrow :
                    this.pMoveUp = true
                    break
                case this.rightArrow :
                    this.pMoveRight = true
                    break
                case this.downArrow :
                    this.pMoveDown = true
                    break
            }
        }
    })
  }

  playerMove = (): void => {
      if(this.pMoveLeft) {
          if (this.canMove('left')) {
              this.moveLeft(this.playerSpeed)
          } else this.respawn()
      }
      if(this.pMoveUp) {
          if (this.canMove('up')) {
              this.moveUp(this.playerSpeed)
          } else this.respawn()
      }
      if(this.pMoveRight) {
          if (this.canMove('right')) {
             this.moveRight(this.playerSpeed)
          } else this.respawn()
      }
      if(this.pMoveDown) {
          if (this.canMove('down')) {
              this.moveDown(this.playerSpeed)
          } else this.respawn()
      }
  }

  moveLeft = (playerSpeed: number): void => {
      if(this.playerPosition.x >= this.mapBounds.xLeft + playerSpeed){
          this.playerPosition.x = this.playerPosition.x - playerSpeed
      } else {
          this.playerPosition.x = this.mapBounds.xLeft
      }
  }

  moveUp = (playerSpeed: number): void => {
      if(this.playerPosition.y >= this.mapBounds.yUp + playerSpeed){
          this.playerPosition.y = this.playerPosition.y - playerSpeed
      } else {
          this.playerPosition.y = this.mapBounds.yUp
      }
  }

  moveRight = (playerSpeed:  number): void => {
      if(this.playerPosition.x <= this.mapBounds.xRight - playerSpeed){
          this.playerPosition.x = this.playerPosition.x + playerSpeed
      } else {
          this.playerPosition.x = this.mapBounds.xRight
      }
  }

  moveDown = (playerSpeed: number): void => {
      if(this.playerPosition.y <= this.mapBounds.yDown - playerSpeed){
          this.playerPosition.y = this.playerPosition.y + playerSpeed
      } else {
          this.playerPosition.y = this.mapBounds.yDown
      }
  }

  canMove = (direction: string): boolean => {
      switch(direction) {
          case 'left' :
              for(let color of this.ctx.getImageData(this.playerPosition.x - 2, this.playerPosition.y, -1,
                this.playerSize.width - 2).data) {
                  if(color == 255) return false

              }
              return true
          case 'right' :
              for(let color of this.ctx.getImageData(this.playerPosition.x + this.playerSize.width + 2, this.playerPosition.y, 1,
                this.playerSize.width - 2).data) {
                  if(color == 255) return false

              }
              return true
          case 'down' :
              for(let color of this.ctx.getImageData(this.playerPosition.x, this.playerPosition.y + this.playerSize.height + 2,
                this.playerSize.height - 2, 1).data){
                  if(color == 255) return false
              }
              return true
          case 'up' :
              for(let color of this.ctx.getImageData(this.playerPosition.x, this.playerPosition.y - 2,
                this.playerSize.height - 2, -1).data){
                  if(color == 255) return false

              }
              return true
      }
  }

  lockPlayerFor500Ms = () => {
    this.playerLocked = true
    this.pMoveDown = this.pMoveLeft = this.pMoveRight = this.pMoveUp = false
    setTimeout(() => { this.playerLocked = false}, 500)
  }

  respawn = () => {
      if (!this.godmode) {
          this.playerPosition.x = 10
          this.playerPosition.y = 10
          this.lockPlayerFor500Ms()
      }
  }

}
