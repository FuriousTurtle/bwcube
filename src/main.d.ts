declare namespace bwCube {
 
    export interface BasicWall { x: number, y: number, width: number, height: number }

    export interface WinningZone { x: number, y: number, width: number, height: number }

    export interface MovingWall {
        x: number, y: number, width: number, height: number, distance: number,
        start: boolean, initX: number, initY: number
    }

    export interface DiagonalWall {
        startX: number, startY: number, endX: number, endY: number,
        thickness: number
    }

    export interface PlayerPosition {
        x: number,
        y: number
    }

    export interface PlayerSize {
        width: number,
        height: number
    }

}
