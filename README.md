BWCube README

In a terminal :
 - Run `npm ci` to install the dependencies as they are defined in the package lock
 - Run `npm run server` in the server folder to compile and start the node server
 - Run `npm start` in the src folder (in another terminal) to compile and start the application

Dev files :
 - **src folder** for typescript source files
 - **public/css folder** for cascading stylesheets
 - **server/src/server.ts file** for node typescript server code
 - **electron folder** for electron files

Upcoming =>
- Customize movements keybindings
- Multiplayer
- Public scoreboard based on a timer
- More maps
- Optimization of the canvas context refresh cycle
- Improvement of the hitbox detection of the cube against moving objects and walls
- New types of walls (spinning, vanishing, etc...)