import * as path from 'path'
import * as express from 'express'
import { Request, Response, Application } from 'express'
const app: Application = express()
const port: number = 8070

app.use(express.static('./public'))

app.get('/index', (_: Request, res: Response) => {
    res.sendFile(path.resolve('public/index.html'))
})

app.listen(port, (err: string) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log(`server is listening on ${port}`)
})
